from __future__ import print_function
import ipywidgets as widgets
import traitlets
from IPython.display import display
from IPython.display import clear_output
import pylab as pb
import os
import ipysheet
import numpy as np

def rename_key(old_dict,old_key,new_key):
    new_dict = {}
    for key, value in zip(old_dict.keys(), old_dict.values()):
        key = key if key != old_key else new_key
        new_dict[key] = value
    return new_dict

materials_dict = dict(
    GaAs=100,
    AlGaAs=101,
    InGaAs=102,
    InSb=200,
    Si=1,
    GaN=300,
    
    Au=1000,
    )

class Region:
    
    def __init__(self, name, material, polygon,
                 #node_sheet_data=None
                 ):
        self._name = name
        self._material = material
        self._polygon = polygon
        #self._node_sheet_data = node_sheet_data
        
    def __str__(self):
        return str(self._name)+','+str(self._material)
    
    @property
    def name(self):
        return self._name
    @property
    def material(self):
        return self._material
    
    #@property
    #def node_sheet_data(self):
    #    return self._node_sheet_data  
    
    @property
    def polygon(self):
        return self._polygon


class AddRegionHandler:
    def __init__(self, main_window=None):
        
        self._main_window = main_window
        _widget = widgets.VBox(
            layout={#'width': '300px', 
            'align_items':'center',
            #'align_content': 'flex-start'
            #'justify_content': 'flex-start'
            }
        )
        self._widget = _widget
        

        layout={'width': '300px',}
        
        _header = widgets.Text(value='Add Region', description='',
                               layout=layout,
                              disabled=True)
        self._header = _header
        
        #num_regions = len(self._main_window.regions.options.keys())
        #name = 'Region '+ str(num_regions+1)
        _name = widgets.Text(value='', description='Name', layout=layout)
        self._name = _name
        
        _material = widgets.Dropdown(description='Material', layout=layout)
        _material.options = materials_dict
        self._material = _material
        
        nodes_widget = widgets.VBox()
        apply_box = widgets.HBox()
        _widget.children = [_header,_name, _material,
                            nodes_widget, apply_box]

        _node_sheet = ipysheet.sheet(key=None, columns=2, column_headers=['X','Y'],
                             layout=layout 
                             )
        self._node_sheet = _node_sheet
        #self.init_random_nodes()
        
        _node_sheet.layout.height = '200px'
        
        #_node_sheet.observe(self.on_nodes_change,'data')
        
        node_sheet_control = widgets.HBox(layout=layout)
        
        
        _xy_add_button = widgets.Button(description='Add')
        #_xy_insert_button = widgets.Button(description='Insert')
        _xy_delete_button = widgets.Button(description='Delete')
        self._xy_delete_button = _xy_delete_button
        
        _xy_delete_button.on_click(self.on_delete_node, remove=False)
        _xy_add_button.on_click(self.on_add_node, remove=False)

        node_sheet_control.children = [
            _xy_add_button,
            #_xy_insert_button, 
            _xy_delete_button]

        nodes_widget.children = [_node_sheet,
                             #box_xy_input,
                             node_sheet_control]
        
        _apply_button = widgets.Button(description='Apply')
        _cancel_button = widgets.Button(description='Cancel')
        apply_box.children = [_apply_button, _cancel_button]
        
        _cancel_button.on_click(self.on_cancel, remove=False)
        _apply_button.on_click(self.on_apply, remove=False)
        
        self._editing = False
        
    
    @property
    def widget(self):
        return self._widget
        
    @property
    def node_sheet(self):
        return self._node_sheet
    @property
    def name(self):
        return self._name
    @property
    def material(self):
        return self._material
    
    @property
    def editing(self):
        return self._editing
    
    @editing.setter
    def editing(self, val):
        self._editing = val
    
    
        
        
    def on_delete_node(self, change):
        if self._node_sheet.rows > 1:
            self._node_sheet.rows -= 1
            
    def on_add_node(self, change):
        self._node_sheet.rows += 1
        
    def on_nodes_change(self, change):
        pass
        #print(change)
        
    def on_cancel(self, change):
        self._main_window.work_window.children = [
            self._main_window.default_window]
        
    def init_random_nodes(self):
        self._node_sheet.rows = 4
        ipysheet.row(0,pb.rand(2).tolist())
        ipysheet.row(1,pb.rand(2).tolist())
        ipysheet.row(2,pb.rand(2).tolist())
        ipysheet.row(3,pb.rand(2).tolist())
        
    def fill_node_sheet(self, xy):
        self._node_sheet.rows = len(xy)
        for i, node in enumerate(xy):
            x, y = node
            ipysheet.row(i, [x,y])

        #print(self._node_sheet.get_state())
        
        #self._validate_node_sheet()
        
    def add_region(self, name, material, nodes, editing=False, **kwargs):

        #print(nodes)
        
        options = dict(self._main_window.regions.options)
        #print("editing = ", editing)
        #print("name = ", name)
        #print("material = ", material)        
        if editing:
            if(name != self._main_window.selected_region.name):
                options = rename_key(options,
                    self._main_window.selected_region.name, name)
            self._main_window.geometry_axes.patches.remove(
                self._main_window.selected_region.polygon)
            index = self._main_window.selected_region_index
            
            poly = self._main_window.selected_region.polygon
            poly.set_xy(nodes)
        else:
            index = len(self._main_window.regions.options)
            poly = pb.Polygon(xy=nodes, closed=True, **kwargs)
        
        self._main_window.geometry_axes.add_patch(poly)
        #self._main_window.geometry_axes.plot(pb.rand(10))
        
        self._main_window.geometry_axes.autoscale(True)
        #pb.show()
        
        self._main_window.geometry_window.clear_output(wait=True)
        with self._main_window.geometry_window:
            display(self._main_window.geometry_fig)
                
        region = Region(name, material, poly)
        options.update({region.name: region})
        
        self._main_window.regions.options = options
        

        self._main_window.regions.index = index
        
        return region
        

    def on_apply(self, change):
        
        #print(self._node_sheet.data)
        self._validate_node_sheet()
        
        
        
        nodes = []
        for node in self._node_sheet.data:
            x,y = (float(cell['value']) for cell in node)
            nodes.append((x,y))
            
            
        #nodes = np.array(nodes)
        
        
        
#         poly = pb.Polygon(xy=nodes, closed=True, ec='k',
#             #fc=fillcolor, fill=fill, zorder=3, lw=self.lw
#         )
        
#         pop = []
#         for i,p in enumerate(self._main_window.geometry_axes.patches):
# 
#             if np.array_equiv(poly.get_xy(),p.get_xy()):
#                 # we are editing        
#                 pop.append(i)
#                 break
                
        #for i in pop:
        #   self._main_window.geometry_axes.patches.pop(i)
            
        
#         options = dict(self._main_window.regions.options)        
#         if self._editing:
#             if(self._name.value != self._main_window.selected_region.name):
#                 options = rename_key(options,
#                     self._main_window.selected_region.name, self._name.value
#                 )
#             self._main_window.geometry_axes.patches.remove(
#                 self._main_window.selected_region.polygon)
#             index = self._main_window.selected_region_index
#         else:
#             index = len(self._main_window.regions.options)
            
        #print(nodes)
        
#         self._main_window.geometry_axes.add_patch(poly)
#         #self._main_window.geometry_axes.plot(pb.rand(10))
#         
#         self._main_window.geometry_axes.autoscale(True)
#         #pb.show()
#         
#         self._main_window.geometry_window.clear_output(wait=True)
#         with self._main_window.geometry_window:
#             display(self._main_window.geometry_fig)
#                 
#         region = Region(self._name.value, self._material.value, poly,
#                         #list(self._node_sheet.data) #make a copy
#                         )
#         options.update({region.name: region})
#         
#         self._main_window.regions.options = options
#         self._main_window.regions.index = index

        
        self.add_region(self._name.value, self._material.value, 
                        nodes, editing=self._editing)

            
        
        self._main_window.work_window.children = [
            self._main_window.default_window]
        
        self._editing = False
            
        

        
    def _validate_node_sheet(self):
        keep = []
        for i, node in enumerate(self._node_sheet.data):
            if node[0]['value'] is not None and node[1]['value'] is not None:
                keep.append(i)
        self._node_sheet.data = [self._node_sheet.data[i] for i in keep]
        
        
        



#with nodes_display:
#        display(nodes_sheet)


