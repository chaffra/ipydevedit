from __future__ import print_function
import ipywidgets as widgets
from IPython.display import display
from IPython.display import clear_output
import pylab as pb
import os
import ipysheet
import traitlets

from .AddRegion import AddRegionHandler, materials_dict
from .menubar import menubar, regions_menu

main_window = widgets.VBox(#layout={'width': '1024px'}
                           )
main_window.selected_region_index = 0

content_window = widgets.HBox()
main_window.children = [menubar, content_window]

geometry_window = widgets.Output(layout={'width': '500px'})


geometry_fig, geometry_axes = pb.subplots()
main_window.geometry_fig = geometry_fig
main_window.geometry_axes = geometry_axes

main_window.geometry_window = geometry_window
work_window = widgets.VBox(#layout={'width': '300px'}
                           )
content_window.children = [geometry_window, work_window]

main_window.work_window = work_window


work_window_layout={'width': '300px',}

default_window = widgets.VBox()
main_window.default_window = default_window
regions = widgets.Select(description='Regions', layout=work_window_layout,
                           options={})
regions.layout.height = '200px'
main_window.regions = regions

regions_control = widgets.HBox(layout=work_window_layout)
default_window.children = [regions, regions_control]

region_add_button = widgets.Button(description='Add')
region_edit_button = widgets.Button(description='Edit')
region_delete_button = widgets.Button(description='Delete')

regions_control.children = [region_add_button,
                            region_edit_button,
                            region_delete_button]



#nodes =[[0.,0.],[0.,1.],[1.,1.]]
#poly = pb.Polygon(nodes)
#geometry_axes.add_patch(poly)
  
@geometry_window.capture(clear_output=True, wait=True)
def init_app():
    #pass
    
    geometry_axes.clear()
    geometry_axes.autoscale(True)
    geometry_axes.invert_yaxis()
    regions.options = {}
    with geometry_window:
        display(geometry_fig)
        
        
        
    work_window.children = [default_window]
        

add_region_handler = AddRegionHandler(main_window)
main_window.add_region_handler = add_region_handler

def on_region_add(change):
    
    add_region_handler.editing = False
    add_region_handler.init_random_nodes()
    
        
    num_regions = len(main_window.regions.options.keys())
    name = 'Region '+ str(num_regions+1)
    add_region_handler.name.value = name
        
    work_window.children = [add_region_handler.widget]
region_add_button.on_click(on_region_add)

def on_region_delete(change):

    region = main_window.selected_region
    main_window.geometry_axes.patches.remove(region.polygon)
    
    main_window.geometry_window.clear_output(wait=True)
    with main_window.geometry_window:
            display(main_window.geometry_fig)
    
    options = dict(main_window.regions.options)
    options.pop(region.name)
    main_window.regions.options = options
region_delete_button.on_click(on_region_delete)
    
def on_region_edit(change):
    region = main_window.selected_region
    
    add_region_handler.editing = True
    add_region_handler.fill_node_sheet(region.polygon.get_xy())
    add_region_handler.name.value = region.name
    add_region_handler.material.value = region.material
        
    work_window.children = [add_region_handler.widget]
region_edit_button.on_click(on_region_edit)
    
def on_regions_menu_change(change):
    if change['new'] == 0:
        return
    elif change['new'] == 1:
        on_region_add(change)
        
    else:
        pass
        #work_window.children = []
    
    #print(change)
    regions_menu.value = 0
regions_menu.observe(on_regions_menu_change,'value')

def on_regions_value_change(change):
    region = change['new']
    main_window.selected_region = region
    pb.setp(main_window.geometry_axes.patches, 'ec', None)
    region.polygon.set_ec('r')
    region.polygon.set_lw(2)
    #print(region)
    
    main_window.geometry_window.clear_output(wait=True)
    with main_window.geometry_window:
            display(main_window.geometry_fig)
    
    
def on_regions_index_change(change):
    main_window.selected_region_index = change['new']
regions.observe(on_regions_value_change, names=['value'], type='change')
regions.observe(on_regions_index_change, names=['index'], type='change')


def add_region(name, material, xy, **kwargs):
    
    #main_window.add_region_handler.fill_node_sheet(xy)
    #main_window.add_region_handler.name.value = name
    material = materials_dict[material]
    region = main_window.add_region_handler.add_region(name,material,xy, editing=False,
                                                       **kwargs)
    return region
    
        
        