from __future__ import print_function
import ipywidgets as widgets

menubar = widgets.HBox()

#
# menubar implementation
#
menu_layout =  {'width': '150px',}
file_menu = widgets.Dropdown(options={'File': 0, 'Load...': 1, 'Save...': 2,},
                             description='', layout=menu_layout)

regions_menu = widgets.Dropdown(value=0, options={
                                            'Regions': 0,
                                            "\tAdd region...": 1,
                                         "\tDelete": 2,
                                        },
                                #description='Regions',
                                layout=menu_layout)

impurities_menu = widgets.Dropdown(options={'Impurities': 0,
                                            'Add impurity...': 1
                                        },
                                description='', layout=menu_layout)

mesh_menu = widgets.Dropdown(options={'Mesh': 0, 'Build...': 1
                                        },
                                description='', layout=menu_layout)

help_menu = widgets.Dropdown(options={'Help': 0, 'Doc': 1
                                        },
                                description='', layout=menu_layout)

menubar.children = [file_menu, regions_menu, impurities_menu, mesh_menu, help_menu]
